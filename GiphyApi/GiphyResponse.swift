//
//  GiphyResponse.swift
//  GiphyApi
//
//  Created by Разработчик on 21.05.2020.
//  Copyright © 2020 Разработчик. All rights reserved.
//

import Foundation

struct DataResponse:Decodable {
    var data:Datum
}
struct Datum:Decodable {
    var images:Images
}

struct Images:Decodable {
    var downsized_large:DownsizedLarge
}

struct DownsizedLarge:Decodable {
    var url:String
}

