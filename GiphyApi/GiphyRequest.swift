//
//  GiphyRequest.swift
//  GiphyApi
//
//  Created by Разработчик on 21.05.2020.
//  Copyright © 2020 Разработчик. All rights reserved.
//

import Foundation

enum GiphyError:Error{
    case noDataAvailable
    case canNotProceedData
}

struct GiphyRequest{
    let resourceURL:URL
    let API_KEY = "qSmUL1vVtDKybqRpjXKCLi5bNqcmFt3L"
    
    init(searchWordImage:String) {
       
       let resourceString = "https://api.giphy.com/v1/gifs/random?api_key=\(API_KEY)&tag=\(searchWordImage)&rating=G"
        
        guard let resourceURL = URL(string: resourceString) else {fatalError()}
        self.resourceURL = resourceURL
    }
    
    func getGiphy (completion: @escaping(Result<Datum,GiphyError>) -> Void ){
        let dataTask = URLSession.shared.dataTask(with: resourceURL) { data, _, _ in
            guard let jsonData = data else{
                completion(.failure(.canNotProceedData))
                return
            }
             DispatchQueue.main.async {
            do {
                let decoder = JSONDecoder()
                let giphyResponse = try decoder.decode(DataResponse.self, from: jsonData)
                let giphyDataDetails = giphyResponse.data
                completion(.success(giphyDataDetails ))
            }catch{
                completion(.failure(.noDataAvailable))
            }
        }
            
        }
        dataTask.resume()
    }
    
}
