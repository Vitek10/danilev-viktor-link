//
//  ViewController.swift
//  GiphyApi
//
//  Created by Разработчик on 20.05.2020.
//  Copyright © 2020 Разработчик. All rights reserved.
//

import UIKit
import RealmSwift

class ViewController: UIViewController, UISearchBarDelegate {
    
    @IBOutlet var searchBar:UISearchBar!
    let realm = try! Realm()
    var searchController: UISearchController!
    var listOfGiphy = Datum.self

    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        searchBarSearchButtonClicked(searchBar)
        getImage()
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchBarText = searchBar.text else {return}
        let giphyRequest = GiphyRequest(searchWordImage: searchBarText)
        giphyRequest.getGiphy { [ weak self ] result in
            switch result{
            case.failure(let error):
                print(error)
            case.success(let images):
                    let gifURL = images.images.downsized_large.url
                    let imageURL = UIImage.gifImageWithURL(gifURL)
                    let imageView = UIImageView(image: imageURL)
                    imageView.frame = CGRect(x: 20.0, y: 220.0, width: self!.view.frame.size.width - 40, height: 150.0)
                    self!.view.addSubview(imageView)
                    print(gifURL)
                    self!.save(gifURL,searchBarText)
            }
        }
    }
    func save(_ imageURL:String, _ text:String){
        let gifs = Gifs()
        gifs.imageRealm = imageURL
        gifs.text = text
        realm.beginWrite()
        realm.add(gifs)
        try! realm.commitWrite()
    }
    
    func getImage(){
           let gifs = realm.objects(Gifs.self)
           for gif in gifs{
               let gifURL = gif.imageRealm
                let imageURL = UIImage.gifImageWithURL(gifURL)
                let imageView = UIImageView(image: imageURL)
                imageView.frame = CGRect(x: 20.0, y: 400.0, width: self.view.frame.size.width - 40, height: 150.0)
               self.view.addSubview(imageView)
           }
       }
    
}

